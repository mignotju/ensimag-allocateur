/*****************************************************
 * Copyright Grégory Mounié 2008-2013                *
 * This code is distributed under the GLPv3 licence. *
 * Ce code est distribué sous la licence GPLv3+.     *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "mem.h"

/*struct ZoneLibre {
    struct ZoneLibre* suiv;
};*/

/** squelette du TP allocateur memoire */

void *zone_memoire = 0;
unsigned long* TZL[BUDDY_MAX_INDEX+1];

unsigned long trouver_indice(unsigned long size) {
    /*unsigned long tmp = size;
    int indice = 0;
    while (tmp % 2 == 0) {
        tmp = tmp >> 1;
        indice=indice + 1;
    }
    for (int i=indice+1; i<BUDDY_MAX_INDEX;i++) {
        tmp=tmp>>1;
        if (tmp % 2 == 1) {
            perror("mem_alloc : size /= 2**i");
        }
    }*/
    int indice=0;
    while (size >1) {
        size = size >> 1;
        indice++;
    }
    return indice;
}

int 
mem_init()
{
  if (! zone_memoire)
    zone_memoire = (void *) malloc(ALLOC_MEM_SIZE);
  if (zone_memoire == 0)
    {
      perror("mem_init:");
      return -1;
    }
  
  for (int i=0; i<BUDDY_MAX_INDEX; i++) {
        TZL[i]=NULL;
  }
  TZL[BUDDY_MAX_INDEX]= zone_memoire;
  *TZL[BUDDY_MAX_INDEX]=0;
  /* ecrire votre code ici */

  return 0;
}

void *
mem_alloc(unsigned long size)
{
    
  /*  ecrire votre code ici */
    
    
    if (size==0 || zone_memoire ==0) {
        perror("mem_alloc: size=0");
        return (void*)0;
    }
    // on calcule l'indice i tel que size=2**i
    unsigned long indice=trouver_indice(size);
    
    
    
   //printf("size : %lu \n",size);
    //printf("indice : %i \n",indice);
    /*unsigned long taille = 0;
    while (taille != size) {*/
        if (TZL[indice] != NULL) {
            unsigned long* tmp = TZL[indice];
            TZL[indice] = (unsigned long*) *tmp;
            return tmp;
        } else {
            int j = indice + 1;
            while ( j <= BUDDY_MAX_INDEX && TZL[j] == NULL ) {
                j++;
            }
            if (j>BUDDY_MAX_INDEX) {
                return (void*)0;
            } else {
                unsigned long* adr = TZL[j];
                TZL[j]=(unsigned long *) *adr;
                while (j>indice) {
                    TZL[j-1]=(unsigned long *) ((unsigned long)adr + (1 << (j-1)));
                    *TZL[j-1]=0;
                    j--;
                }
                return adr;
            }
            //taille de la case TZL[j]
            /*taille = 1;
            for (int i = 1; i <= j; i++) {
                taille = taille << 1;
            }
            
            int* adr_j=TZL[j];
            int* adr_j_1=TZL[j-1];

            TZL[j - 1] = (*adr_j)^taille;
            //taille divisee par deux
            taille = taille >> 1;
            (TZL[j - 1].suiv) = (*adr_j_1)^taille;
            (TZL[j - 1].suiv)->suiv = NULL;

            if (taille == size) {
                return TZL[j - 1];
            }*/
        }
    //}
    
  return 0;  
}

int 
mem_free(void *ptr, unsigned long size)
{
  /* ecrire votre code ici */
  return 0;
}


int
mem_destroy()
{
  /* ecrire votre code ici */

  free(zone_memoire);
  zone_memoire = 0;
  return 0;
}

